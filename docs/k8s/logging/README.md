# Logging with Elasticsearch, Fluentd and Kibana
https://www.digitalocean.com/community/tutorials/how-to-set-up-an-elasticsearch-fluentd-and-kibana-efk-logging-stack-on-kubernetes

```shell script
linkerd inject k8s/logging/kube-logging.yml | kubectl create -f -

kubectl get namespaces
kubectl create -f k8s/logging/elasticsearch_svc.yml
kubectl get services --namespace=kube-logging

linkerd inject k8s/logging/elasticsearch_statefulset.yml | kubectl create -f -
kubectl rollout status sts/es-cluster --namespace=kube-logging

kubectl port-forward es-cluster-0 9200:9200 --namespace=kube-logging
curl http://localhost:9200/_cluster/state?pretty

linkerd inject k8s/logging/kibana.yml | kubectl create -f -
kubectl rollout status deployment/kibana --namespace=kube-logging

kubectl get pods --namespace=kube-logging
kubectl port-forward kibana-6c9fb4b5b7-plbg2 5601:5601 --namespace=kube-logging
```
http://localhost:5601

# Fluentd
```shell script
linkerd inject k8s/logging/fluentd.yml | kubectl create -f -
kubectl get ds --namespace=kube-logging

```
http://localhost:5601

```shell script
linkerd inject k8s/logging/counter.yml | kubectl create -f -
linkerd inject k8s/logging/counter.yml | kubectl delete -f -
```