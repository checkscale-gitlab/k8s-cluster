variable "do_token" {}
variable "do_region" {
  default = "sfo2"
}
locals {
  do_vpc_name = "k8s-cluster-testing"
  do_vpc_id   = data.digitalocean_vpc.k8s-cluster-testing.id
}

# Cluster Settings
variable "do_k8s_version" {
  default = "1.16.8-do.0"
}

# Node Settings
variable "do_k8s_node_size" {
  default = "s-2vcpu-4gb"
}
variable "do_k8s_node_pool_prefix" {
  default = "do-k8s-pool"
}
variable "do_k8s_node_count" {
  default = 3
}
variable "do_k8s_node_min" {
  default = 3
}
variable "do_k8s_node_max" {
  default = 3
}